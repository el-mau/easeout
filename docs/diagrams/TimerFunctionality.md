# Timer Functionality

This document contains information about the flow of calls and states that should occur when the timer is stopped.

## Flow of Timer UX

### Submit Timer Config

```mermaid
flowchart TD
    A[Start] --> B(Configure Session)
    B --> C(Click Submit)
    C --> app(App.vue)
    app --- |store| tw[/Work Time/]
    app --- |store| tr[/Rest Time/]
    C ---> D(Go to Timer Screen)
    D --> E[End]
```

### Timer Use

```mermaid
flowchart TD
    A[Start] --> B(Click Start)
    B --> C(Timer Starts Running)
    C --> pause{User Clicks on Pause}
    pause --> |Yes| E(Timer Stops)
    pause --> |No| skip{User Clicks on Skip}
    skip --> |Yes| F(Timer Resets with new Time)
    F --> resume
    skip --> |No| H(Timer Runs Until Done)
    E --> resume{Click Start}
    resume --> |Yes| H
    resume --> |No| J[Timer Stays Stopped]
    resume --> pause
    H --> I[End]
```

### Reconfigure Session

```mermaid
flowchart TD
    A[Start] --> B(Click On Session Configuration)
    B --> C(Go Back to Session Configuration)
    C --> cancel{Click on Cancel}
    cancel --> |Yes| D(Go to Timer Screen)
    cancel --> |No| F(Modify Values)
    F --> app(App.vue)
    app --- |update| tw[/Work Time/]
    app --- |update| tr[/Rest Time/]
    F --> D
    D --> E[End]
```

## Timer Time is set on Timer End Event

```mermaid
sequenceDiagram
  timerInstance->>App: emitEvent(timerEnded)
  App->>App: handleTimerSwitch()
  App->>timerInstance: setSound()
  App->>timerInstance: stopTimer()
  timerInstance->>timerInstance: pauseTimer()
  timerInstance->>timerInstance: restartTimer()
```
