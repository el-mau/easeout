import { useWakeLock } from "@vueuse/core";

const { isSupported, request, release } = useWakeLock();

const lockScreen = () => {

  if (isSupported) {
    request("screen");
  }
};

const unlockScreen = () => {
  if (isSupported) {
    release();
  }
}

export { lockScreen, unlockScreen };
