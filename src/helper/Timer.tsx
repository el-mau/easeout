export default class Timer {

  timerId: any;
  initialSeconds: number;
  seconds: number;

  constructor() {
    this.timerId = null;
    this.initialSeconds = 0;
    this.seconds = this.initialSeconds;
  }

  startTimer() {
    return new Promise<void>((resolve) => {
      this.timerId = setInterval(() => {
        this.seconds++;
        console.log(this.seconds);
      }, 1000);
      resolve();
    });
  }

  stopTimer() {
    clearInterval(this.timerId);
  }

  pauseTimer() {
    this.stopTimer();
  }

  resetTimer() {
    this.seconds = this.initialSeconds;
  }

  modifyTime(value: number) {
    return new Promise<void>((resolve) => {
      this.pauseTimer();
      this.seconds = this.initialSeconds = value;
      resolve();
    });
  }

}

