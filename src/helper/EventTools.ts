const addCallbackToEvents = (element: Element, events: string[], callback: () => void) => {
  events.forEach(event => {
    element.addEventListener(event, callback);
  });
}

const disableContextMenu = () => {
  window.oncontextmenu = function(event) {
     event.preventDefault();
     event.stopPropagation();
     return false;
  };
};

export {
  addCallbackToEvents,
  disableContextMenu
};
