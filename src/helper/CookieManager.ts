import Cookies from 'js-cookie';
import type { CookieAttributes } from 'node_modules/@types/js-cookie';

const cookieOptions: CookieAttributes = {
  expires: 365,
  sameSite: 'strict',
};

const setCookie = (cookieName: string, cookieValue: any) => {
  Cookies.set(cookieName, cookieValue, cookieOptions);
}

const setNumberCookie = (cookieName: string, cookieValue: number) => {
  Cookies.set(cookieName, cookieValue.toString(), cookieOptions);
}

const setDateCookie = (cookieName: string, cookieValue: Date) => {
  Cookies.set(cookieName, cookieValue.toISOString(), cookieOptions);
}

const setBooleanCookie = (cookieName: string, cookieValue: boolean) => {
  Cookies.set(cookieName, cookieValue ? 'true' : 'false', cookieOptions);
}

const getCookie = (cookieName: string): string => {
  return Cookies.get(cookieName) || '';
}

const getCookieAsNumber = (cookieName: string) => {
  return parseInt(Cookies.get(cookieName) || '0');
}

const getCookieAsDate = (cookieName: string) => {
  return new Date(Cookies.get(cookieName) || '');
}

const getBooleanCookie = (cookieName: string) => {
  return Cookies.get(cookieName) === 'true';
}

const removeCookie = (name: string) => {
  console.debug(`Removing cookie: ${name}`);
  Cookies.remove(name, { sameSite: 'strict' });
}

export {
  setCookie,
  setNumberCookie,
  setDateCookie,
  setBooleanCookie,
  getCookie,
  getCookieAsNumber,
  getCookieAsDate,
  getBooleanCookie,
  removeCookie,
};

