import { mount } from '@vue/test-utils';
import { expect, describe, test } from 'vitest';
import PopupWindow from '../PopupWindow.vue';

test('hello world', () => {
  expect(1).toBe(1);
});

/*
describe('PopupWindow renders with default message and hidden', async () => {
  const wrapper = mount(PopupWindow);

  // Assert the default message is displayed
  expect(wrapper.find('p').text()).toBe('Default Message');

  // Assert the popup is initially hidden
  expect(wrapper.html()).not.toContain('popup-window');
});

describe('PopupWindow shows and hides on toggle', async () => {
  const wrapper = mount(PopupWindow);

  // Click to open the popup
  await wrapper.find('.close-btn').trigger('click');

  // Assert that the popup is visible
  expect(wrapper.html()).toContain('popup-window');

  // Click to close the popup
  await wrapper.find('.close-btn').trigger('click');

  // Assert that the popup is hidden again
  expect(wrapper.html()).not.toContain('popup-window');
});
*/
