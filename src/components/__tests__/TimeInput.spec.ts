import { mount } from '@vue/test-utils'
import { assert, test } from 'vitest'
import TimeInput from '@/components/TimeInput.vue'

test('Slider input exists', () => {
  const wrapper = mount(TimeInput)
  const rangeInput = wrapper.find('#slider')
  assert.ok(rangeInput.exists())
});

test('Decrement proportion value', async () => {
  const wrapper = mount(TimeInput)
  const slider = wrapper.find('#slider')
  const sliderLabel = wrapper.find('.slider-label')
  const hoursPlusButton = wrapper.find('#hours-container .plus-button')

  assert.ok(slider.exists())
  assert.ok(sliderLabel.exists())
  assert.ok(hoursPlusButton.exists())
  // before decrement
  assert.ok(sliderLabel.text().includes, '70%') 
  assert.ok(sliderLabel.text().includes, '30%') 

  await hoursPlusButton.trigger('click') // add hour to session
  await slider.setValue('50')
  assert.ok(sliderLabel.text().includes('50%'))
  assert.ok(wrapper.find('#time-calculation-container').text().includes('Work - 00:30:00'))
});

test('Increment proportion value', async () => {
  const wrapper = mount(TimeInput)
  const slider = wrapper.find('#slider')
  const sliderLabel = wrapper.find('.slider-label')
  const hoursPlusButton = wrapper.find('#hours-container .plus-button')
  const timeCalcContainer = wrapper.find('#time-calculation-container')

  assert.ok(slider.exists())
  assert.ok(sliderLabel.exists())
  assert.ok(hoursPlusButton.exists())
  // before increment
  assert.ok(sliderLabel.text().includes, '70%') 
  assert.ok(sliderLabel.text().includes, '30%') 

  await hoursPlusButton.trigger('click') // add hour to session
  await slider.setValue('100')
  assert.ok(sliderLabel.text().includes('100%'))
  assert.ok(sliderLabel.text().includes('0%'))
  assert.ok(timeCalcContainer.text().includes('Work - 01:00:00'))
});

test('Increment buttons exists', () => {
  const wrapper = mount(TimeInput)
  assert.ok(wrapper.find('#hours-container .plus-button').exists())
  assert.ok(wrapper.find('#minutes-container .plus-button').exists())
});

test('Decrement buttons exists', () => {
  const wrapper = mount(TimeInput)
  assert.ok(wrapper.find('#hours-container .minus-button').exists())
  assert.ok(wrapper.find('#minutes-container .minus-button').exists())
});

test('Hours and minutes labels for buttons exists', () => {
  const wrapper = mount(TimeInput)
  const labels = wrapper.findAll('.time-input-main .top-section label')
  assert.equal(labels[0].text(), 'Hours')
  assert.equal(labels[1].text(), 'Session Time')
  assert.equal(labels[2].text(), 'Minutes')
});

test('Hours and minutes display exists', () => {
  const wrapper = mount(TimeInput)
  const timeDisplayedOnScreen = wrapper.find('.session-time-input-display p')
  assert.ok(timeDisplayedOnScreen.text() === '00:00')
});

test('Info for session is displayed', () => {
  const wrapper = mount(TimeInput)
  const sessionInfoContainer = wrapper.find('#time-calculation-container')
  // TODO: add formatted time to this
  assert.ok(sessionInfoContainer.text().includes('Session -'))
  assert.ok(sessionInfoContainer.text().includes('Work -'))
  assert.ok(sessionInfoContainer.text().includes('Rest -'))
});

test('Increment hours and minutes from 0', async () => {
  const wrapper = mount(TimeInput);
  await wrapper.find('#hours-container .plus-button').trigger('click')
  await wrapper.find('#minutes-container .plus-button').trigger('click')
  assert.equal(wrapper.find('.session-time-input-display p').text(), '01:01')
});

test('Decrement hours and minutes from 0', async () => {
  const wrapper = mount(TimeInput);
  await wrapper.find('#hours-container .minus-button').trigger('click')
  await wrapper.find('#minutes-container .minus-button').trigger('click')
  assert.equal(wrapper.find('.session-time-input-display p').text(), '23:59') // 22 because I decrement hours first
});

test('Decrement hours and minutes on keyhold', async () => {
  const wrapper = mount(TimeInput);
  const hoursMinusButton = wrapper.find('#hours-container .minus-button')
  const minutesMinusButton = wrapper.find('#minutes-container .minus-button')
  const timeContainer = wrapper.find('.session-time-input-display p')

  assert.ok(timeContainer.text() === '00:00')

  await minutesMinusButton.trigger('mousedown')
  await hoursMinusButton.trigger('mousedown')
  await new Promise(resolve => setTimeout(resolve, 1010));
  await hoursMinusButton.trigger('mouseup')
  await minutesMinusButton.trigger('mouseup')

  assert.equal(timeContainer.text(), '18:54')
});

test('Increment hours and minutes on keyhold', async () => {
  const wrapper = mount(TimeInput);
  const hoursPlusButton = wrapper.find('#hours-container .plus-button')
  const minutesPlusButton = wrapper.find('#minutes-container .plus-button')
  const timeContainer = wrapper.find('.session-time-input-display p')

  assert.ok(timeContainer.text() === '00:00')

  await hoursPlusButton.trigger('mousedown')
  await minutesPlusButton.trigger('mousedown')
  await new Promise(resolve => setTimeout(resolve, 1010));
  await hoursPlusButton.trigger('mouseup')
  await minutesPlusButton.trigger('mouseup')

  assert.equal(timeContainer.text(), '06:06')
});

