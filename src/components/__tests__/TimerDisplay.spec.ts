import { mount } from '@vue/test-utils'
import { assert, test } from 'vitest'
import TimerDisplay from '@/components/TimerDisplay.vue'

test('Hours and minutes selector is displayed', () => {
  const wrapper = mount(TimerDisplay)
  const timeSelectionDisplay = wrapper.find('p.time')
  assert.ok(timeSelectionDisplay.exists())
  timeSelectionDisplay.text().includes('00:00')
});

test('Timer title is displayed', () => {
  const wrapper = mount(TimerDisplay)
  const timerTitle = wrapper.find('h1.timer-title')
  assert.ok(timerTitle.exists())
  assert.equal(timerTitle.text(), '')
});

test('Set title and verify display', async () => {
  const wrapper = mount(TimerDisplay)
  const timerTitle = wrapper.find('h1.timer-title')

  assert.ok(timerTitle.exists())
  assert.equal(timerTitle.text(), '')

  // NOTE: removing await causes test to fail, maybe ref.value changes require await
  await wrapper.vm.setTitle('Test Title')
  assert.equal(timerTitle.text(), 'Test Title')
});

test('Set time, start and pause timer', async () => {
  const wrapper = mount(TimerDisplay)
  const timeDisplay = wrapper.find('p.time')

  assert.ok(timeDisplay.exists())
  assert.equal(timeDisplay.text(), '00:00:00')

  // NOTE: removing await causes test to fail, maybe ref.value changes require await
  const newTime = 2 * 60 * 60 // 2 hours
  await wrapper.vm.setTime(newTime)
  assert.equal(timeDisplay.text(), '02:00:00')

  await wrapper.vm.startTimer()
  await new Promise(resolve => setTimeout(resolve, 1000))
  assert.equal(timeDisplay.text(), '01:59:59')
});

