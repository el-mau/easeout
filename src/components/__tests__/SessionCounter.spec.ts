import { mount } from '@vue/test-utils'
import { assert, test } from 'vitest'
import SessionCounter from '@/components/SessionCounter.vue'
import MockButton from '@/components/__tests__/MockButton.vue'

// credit: https://stackoverflow.com/questions/179355/clearing-all-cookies-with-javascript
function deleteAllCookies() {
    const cookies = document.cookie.split(";");

    cookies.forEach(cookie => {
      const key = cookie.split('=')[0];
      document.cookie = key + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT';
    });
}

test('SessionCounter exists', () => {
  const wrapper = mount(SessionCounter)
  assert.ok(wrapper.find('.session-count-container').exists())
  assert.equal(wrapper.find('.session-count-title').text(), 'Session Count')
  assert.equal(wrapper.find('.session-count').text(), '0')
});

test('Increment session count', async () => {
  const wrapper = mount(SessionCounter)
  const mockButton = mount(MockButton)

  // @ts-ignore
  mockButton.vm.setHandler(wrapper.vm.incrementSessionCount)
  await mockButton.find('button').trigger('click')
  assert.equal(wrapper.find('.session-count').text(), '1')
});

test('Increment session count and check time of day session count', async () => {
  deleteAllCookies()
  const wrapper = mount(SessionCounter)
  const mockButton = mount(MockButton)
  const today = new Date()

  // @ts-ignore
  mockButton.vm.setHandler(wrapper.vm.incrementSessionCount)

  await mockButton.find('button').trigger('click')
  assert.equal(wrapper.find('.session-count').text(), '1')

  // TODO: make this more UX oriented
  if (today.getHours() < 12) {
    assert.equal(wrapper.vm.getMorningSessionCount(), 1)
  }
  if (today.getHours() >= 12 && today.getHours() < 18) {
    assert.equal(wrapper.vm.getAfternoonSessionCount(), 1)
  }
  if (today.getHours() >= 18) {
    assert.equal(wrapper.vm.getEveningSessionCount(), 1)
  }
});
