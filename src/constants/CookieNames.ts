export const SESSION_COUNT_COOKIES = {
  totalCount: 'totalSessionCount',
  morningCount: 'sessionCountMorning',
  afternoonCount: 'sessionCountAfternoon',
  eveningCount: 'sessionCountEvening',
  lastSessionDate: 'lastSessionDate',
};

export const TIMER_COOKIES = {
  initialTime: 'timerInitialTime',
  time: 'timerCurrentTime',
  title: 'timerCurrentTitle',
};

export const SESSION_CONFIG_COOKIES = {
  isConfigured: 'sessionIsConfigured',
  sessionTime: 'totalSessionTimeConfigured',
  workProportion: 'workProportion',
}
