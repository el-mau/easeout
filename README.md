# easeout

Ease out of laziness or ease out of overworking.

## Description

This is a timer that sets how much time we dedicate towards work and how much time to rest. It is inspired by the pomodoro technique.

The main idea is this:

1. Set a time for a total "session of productivity"
2. Set the percentage of time you're dedicating to work and to rest

Example:

- Total session: 00:35 (35 minutes)
- Work percentage: 85% of total session (29 minutes and 45 seconds)
- Work percentage: 15% of total session (5 minutes and 15 seconds)

## App Images

![Alt text](./docs/images/screen1.png)
![Alt text](./docs/images/screen2.png)
